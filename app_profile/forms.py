from django import forms
import datetime

LEVEL_CHOICES = (
	('',''),
	('Beginner','Beginner'),
	('Intermediate','Intermediate'),
	('Advanced','Advanced'),
	('Expert','Expert'),
	('Legend','Legend'),
)

class Friend_Form(forms.Form):
    NPM = forms.CharField(label='NPM', required=True, max_length=10)
    Nama = forms.CharField(label='Nama', required=True, )
    Angkatan =  forms.ChoiceField(choices=[(x, x) for x in range(1900, 2018)])
    Skill1 = forms.CharField(required=False)
    Skill2 = forms.CharField(required=False)
    Level1 = forms.ChoiceField(choices=LEVEL_CHOICES, required=False)
    Level2 = forms.ChoiceField(choices=LEVEL_CHOICES, required=False)
# Create your views here.
class Message_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, max_length=50)