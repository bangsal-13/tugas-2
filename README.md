## PPW - [TUGAS 2](https://bangsal-13.herokuapp.com)
Projek ini dibuat untuk memenuhi Tugas 2 mata kuliah PPW Semester Ganjil 2017/2018

### Pipeline and Coverage
[![pipeline status](https://gitlab.com/bangsal-13/tugas-2/badges/master/pipeline.svg)](https://gitlab.com/bangsal-13/tugas-2/commits/master)
[![coverage report](https://gitlab.com/bangsal-13/tugas-2/badges/master/coverage.svg)](https://gitlab.com/bangsal-13/tugas-2/commits/master)

### Members
- [**Aviliani Pramestya**](https://gitlab.com/AvilianiPramestya) - 1606829402
- [**Imran Amrulloh**](https://gitlab.com/iamrulloh) - 1606828942
- [**Juan Alexander Hamonangan**](https://gitlab.com/pakpahanjuan46) - 1606839334
- [**Teuku Muhammad Farhan Syafiq**](https://gitlab.com/farhan_syafiq) - 1606882276