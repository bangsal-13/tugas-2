from django.conf.urls import url
from .views import index, stats, add_friend, add_status, edit_profile

# /sol
from .custom_auth import auth_login, auth_logout

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_friend', add_friend, name='add_friend'),
    url(r'^home/$', stats, name='stats'),
    url(r'^add_status', add_status, name='add_status'),

    url(r'^mahasiswa/$', stats, name='stats'),
    url(r'^mahasiswa/profile/edit$', edit_profile, name='edit_profile'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]