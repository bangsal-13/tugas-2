var lang = [
  {"id":0,"text":"Java"},
  {"id":1,"text":"C#"},
  {"id":2,"text":"Python"},
  {"id":3,"text":"Erlang"},
  {"id":4,"text":"Kotlin"},
];

var level = [
  {"id":0,"text":"Beginner","fontColor":"#FAFAFA"},
  {"id":1,"text":"Intermediate","fontColor":"#FAFAFA"},
  {"id":2,"text":"Advance","fontColor":"#FAFAFA"},
  {"id":3,"text":"Expert","fontColor":"#FAFAFA"},
  {"id":4,"text":"Legend","fontColor":"#212121"},
];

$(document).ready(function() {
  localStorage.setItem("lang",JSON.stringify(lang));
  localStorage.setItem("level",JSON.stringify(level));

  $('#lang').select2({
    'placeholder' : "Select Language",
    'data' : select_lang = JSON.parse(localStorage.getItem("lang"))
  });

  $('#expertise').select2({
      placeholder : "Select Expertise",
      data : select_expertise = JSON.parse(localStorage.getItem("level"))
  });


});
