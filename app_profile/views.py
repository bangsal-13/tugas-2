from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import Friend_Form
from .models import Teman
from .riwayat_api import get_riwayat
from .forms import Message_Form
from .models import Status



response = {'author':'kelompok 13'}

def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('app:stats'))
    html = 'app_profile/session/login.html'
    return render(request, html, response)

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['friend_form'] = Friend_Form
    teman = Teman.objects.all()
    response['teman'] = teman 
    # response['mahasiswa'] =
    response['riwayats'] = get_riwayat()

    ## handling agar tidak error saat pertama kali login (session kosong)
    if 'drones' in request.session.keys():
        response['fav_drones'] = request.session['drones']
    # jika tidak ditambahkan else, cache akan tetap menyimpan data
    # sebelumnya yang ada pada response, sehingga data tidak up-to-date
    else:
        response['fav_drones'] = []

def stats(request):
    ## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('app:index'))

    set_data_for_session(response, request)
    status = Status.objects.all().order_by('-created_date')
    response['status'] = status
    response['message_form'] = Message_Form


    html = 'app_profile/session/stats.html'
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['NPM'] = request.POST['NPM']
        response['Nama'] = request.POST['Nama']
        response['Angkatan'] = request.POST['Angkatan']
        response['Skill1'] = request.POST['Skill1']
        response['Skill2'] = request.POST['Skill2']
        response['Level1'] = request.POST['Level1']
        response['Level2'] = request.POST['Level2']
        friend = Teman(NPM=response['NPM'], Nama=response['Nama'], Angkatan=response['Angkatan'], Skill1=response['Skill1'], Skill2=response['Skill2'], Level1=response['Level1'], Level2=response['Level2'])
        friend.save()
        return HttpResponseRedirect(reverse('app:index'))
    else:
        return HttpResponseRedirect(reverse('app:index'))

def add_status(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        status = Status(message=response['message'])
        status.save()
        html ='app_profile/session/stats.html'
        render(request, html, response)
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/add_status/')

def database(request):
    name = request.POST['name']
    email = request.POST['email']
    profile_url = request.POST['profileUrl']
    image_url   = request.POST['imageUrl']

    request.user.profile.kode_identitas

    request.user.profile.update({
        'name': name,
        'image_url': image_url,
        'email': email,
        'profile_url': profile_url,
    })
    return JsonResponse({'save':'success'})

def edit_profile(request):
    html = 'app_profile/session/update_profile.html'
    return render(request, html, response)
