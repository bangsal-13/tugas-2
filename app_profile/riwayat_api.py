import requests

RIWAYAT_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def get_riwayat():
	riwayat = requests.get(RIWAYAT_API)
	return riwayat.json()